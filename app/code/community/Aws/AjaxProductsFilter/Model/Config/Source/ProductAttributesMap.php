<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Model_Config_Source_ProductAttributesMap
{
    public static $rulesColumns = array();
    protected static $_postProcessed = false;

    protected static function loadProductAttributes()
    {
        try {
            static::$rulesColumns['attribute']['values'] =
                Aws_AjaxProductsFilter_Model_Config_Source_ProductAttributes::toOptionArrayStatic();
        } catch (Exception $exception) {}
    }


    public function toOptionArray()
    {
        if (!static::$_postProcessed) {
            static::$rulesColumns = array(
                'attribute' => array(
                    'label' => 'Product Attribute',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_SELECT,
                    'style' => 'width:120px',
                    'values' => array()
                ),
                'alias' => array(
                    'label' => 'Attribute Alias',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
                    'style' => 'width:120px'
                )
            );
            static::loadProductAttributes();
        }
        return static::$rulesColumns;
    }
}