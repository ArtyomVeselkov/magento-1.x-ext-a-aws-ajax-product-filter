<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Model_Config_Source_ProductAttributes
{
    protected static $_postProcessed = false;

    protected static function loadProductAttributes()
    {
        try {
            $attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
            $attributesStack = array();
            foreach ($attributes as $attribute) {
                $attributesStack[] =
                    array(
                        'value' => $attribute->getAttributecode(),
                        'label' => sprintf(
                            '%s [%s]',
                            $attribute->getAttributeCode(),
                            $attribute->getFrontendLabel()
                        )
                    );
            }
            static::$_postProcessed = $attributesStack;
        } catch (Exception $exception) {}
    }

    public static function toOptionArrayStatic()
    {
        if (!static::$_postProcessed) {
            static::loadProductAttributes();
        }
        return static::$_postProcessed;
    }

    public function toOptionArray()
    {
        return static::toOptionArrayStatic();
    }
}