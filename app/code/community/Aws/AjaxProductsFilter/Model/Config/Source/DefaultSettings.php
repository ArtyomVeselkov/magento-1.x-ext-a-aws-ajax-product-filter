<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Model_Config_Source_DefaultSettings
{
    public static $rulesColumns = array();
    protected static $_postProcessed = false;

    protected static function loadProductAttributes()
    {
        try {
            $defaultParameters = Aws_AjaxProductsFilter_Helper_Data::getDefaultParameters();
            static::$rulesColumns['parameter']['values'] = array_map(
                function ($item) {
                    return array(
                        'label' => $item,
                        'value' => $item
                    );
                },
                $defaultParameters
            );
        } catch (Exception $exception) {}
    }


    public function toOptionArray()
    {
        if (!static::$_postProcessed) {
            static::$rulesColumns = array(
                'parameter' => array(
                    'label' => 'Parameter',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_SELECT,
                    'style' => 'width:120px',
                    'values' => array()
                ),
                'value' => array(
                    'label' => 'Value',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
                    'style' => 'width:120px'
                )
            );
            static::loadProductAttributes();
        }
        return static::$rulesColumns;
    }
}