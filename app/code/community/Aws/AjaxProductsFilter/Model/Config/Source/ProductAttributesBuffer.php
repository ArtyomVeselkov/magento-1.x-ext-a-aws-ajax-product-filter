<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Model_Config_Source_ProductAttributesBuffer
{
    public static $rulesColumns = array();
    protected static $_postProcessed = false;

    protected static function loadProductAttributes()
    {
        try {
            static::$rulesColumns['attribute']['values'] =
                Aws_AjaxProductsFilter_Model_Config_Source_ProductAttributes::toOptionArrayStatic();
        } catch (Exception $exception) {}
    }


    public function toOptionArray()
    {
        if (!static::$_postProcessed) {
            static::$rulesColumns = array(
                'attribute' => array(
                    'label' => 'Product Attribute',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_SELECT,
                    'values' => array()
                ),
                'regexp' => array(
                    'label' => 'Pattern (RegExp)',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
                ),
                'replacement' => array(
                    'label' => 'Replacement (RegExp)',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
                ),
                'type' => array(
                    'label' => 'Data Type',
                    'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_SELECT,
                    'values' => array(
                        array(
                            'value' => 'string',
                            'label' => 'string'
                        ),
                        array(
                            'value' => 'numeric',
                            'label' => 'numeric'
                        )
                    )
                ),
            );
            static::loadProductAttributes();
        }
        return static::$rulesColumns;
    }
}