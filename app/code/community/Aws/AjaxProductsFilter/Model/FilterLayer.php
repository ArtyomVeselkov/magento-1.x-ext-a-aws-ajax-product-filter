<?php

class Aws_AjaxProductsFilter_Model_FilterLayer extends Mage_Catalog_Model_Layer
{
    protected $_actualCollection = null;

    public function setCollection($collection)
    {
        $this->_actualCollection = $collection;
    }

    public function getProductCollection()
    {
        return $this->_actualCollection;
    }
}