<?php

class Aws_AjaxProductsFilter_Model_Observer
{
    const CLEAN_MARK = '<!-- cleaned block -->';

    protected static $_skipHtmlBlocks = array();
    protected static $_enableSkip = false;

    public static function skipBlock($alias, $skipFlag = true)
    {
        if (is_int($alias) || is_string($alias)) {
            static::$_skipHtmlBlocks[$alias] = $skipFlag;
        }
    }

    public static function toggleMode($enable = false)
    {
        static::$_enableSkip = $enable;
    }

    public function processSkip($event)
    {
        try {
            if (!static::$_enableSkip || !count(static::$_skipHtmlBlocks)) {
                return ;
            }
            $transport = $event->getTransport();
            $block = $event->getBlock();
            if (
                @is_a($block, 'Mage_Core_Block_Template') &&
                @is_a($transport, 'Varien_Object') &&
                isset(static::$_skipHtmlBlocks[$block->getBlockAlias()])
            ) {
                $counter = static::$_skipHtmlBlocks[$block->getBlockAlias()];
                if ($counter) {
                    $transport->setHtml(self::CLEAN_MARK);
                    if (is_int($counter)) {
                        static::$_skipHtmlBlocks[$block->getBlockAlias()] = $counter - 1;
                    }
                }
            }
        } catch (Exception $exception) {}
    }
}