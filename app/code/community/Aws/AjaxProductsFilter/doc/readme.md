AWS AjaxProductsFilter Module
=============================

# 1. Perform Ajax Query
## 1.1 Request Example

```json
{
  "form_key": "xxxxxxx",
  "attributes": [
    {
      "name": "size",
      "operator": "eq",
      "value": 1.5
    },
    {
      "name": "b",
      "operator": "[between",
      "value": [6000, 15000]
    }
  ],
  "mode": "html-template",
  "force_load": false
}
```

**Possible operators:**
  - eq/neq;
  - null/notnull;
  - in/nin;
  - like/nlike;
  - between/between]/[between/[between]
  - gt/lt/gteq/lteq
  
All filters supplied to query will be `AND` concatenated.

## 1.2 Response Example

If request was successful.
```json
{
  "status": "1",
  "content": "<div class=\"content\">... content here ...</div>",
  "info": {
    "cache": "Cache exists, but force load parameter is specified.",
    "products_count": "30"
  }
}
```

# 2. Configuration
## 2.1 General Settings

  - **Specify attributes, which will be accessible for queries**
    
    Only selected attributes will be accessible via POST requests. If non-selected attribute
      is supplied into POST requests -- error will be returned.
      
  - **Default settings**
  
    Default settings will override "out-of-the-box settings". For example, specify `limit` = 30, if You want 
      to display only first 30 found products (or set it to `all` -- to show all products).
      
## 2.2 Ajax Related Settings
 
  - **Enable fields (attributes) mapping**
   
    Set it to "Yes" if You want to mask real attributes' codes and use specified alias in requests.

  - **Manage Attribute - Alias Map**
  
    Specify pairs "attribute -- it's alias".
    
## 2.3 Advanced

   - **Specify RegExp replacements for attributes' values**
   
     In case of attributes that use options (labeled values) for quantitative values / are
       non-standardized -- use this grid to specify RegExp rules of converting labels into
       comparable array.
       
     *Example*
     Attribute size have "7" labeled variants (1):
     - 1.5 Ton
     - 2.0 Ton
     - 2.5 Ton
     - 3.0 Ton
     - 3.5 Ton
     - 4.0 Ton
     - 5.0 Ton
     
     We want to get all products with [2, 3.5) tones.
     
     As Magento (c) holds all labeled options like (value = 95, label = "3.0 Ton"),
      we need intermediate array.
       
     Those, (1) is converted to (2):
     - 1.5 Ton = 98
     - 2.0 Ton = 97
     - 2.5 Ton = 96
     - 3.0 Ton = 95
     - 3.5 Ton = 94
     - 4.0 Ton = 93
     - 5.0 Ton = 92
     
     After that we compare queried value with (2) and replace comparative operators (`gt`, `lt`) with `IN`.
     
   - **Recommended JS *(for HTML answer)***
     
     Code that what be attached to `info.recommended_js` field in response for execution via
   `eval`.
   
   *Example*
   ```
   try {
       jQuery('.actions a, .actions button').tooltip();
   } catch (e) {}
   ```
   - **Append recommended JS to answer *(for HTML answer)***
    
     Specified code will be injected at the bottom of response body (along with `info.recommended_js`).

   - **Used handles for HTML-Template rendering**
   
     Specified via comma / new line handles' names, which would be used for rendering products' list block.
     
     *Example*
     ```
        catalog_category_layered_nochildren,
        catalog_category_layered,
        catalog_category_default,
        catalog_category_view,
        default
     ```

# 3 Usage example for developers

# 3.1 Sample code with jQuery
```html
<div id="aws-target-product-list">
</div>

<script type="text/javascript">
    var uri = window.location.protocol + '//' + window.location.host + '/aws-apf/search/execute',
        jsonStrJson = {
            "form_key": typeof window.awsWidgetCrossStaticK !== 'undefined' ? window.awsWidgetCrossStaticK : 'xxxxx',
            "attributes": [
                {
                    "name": "s",
                    // means 'size'
                    "operator": "[between]",
                    "value": [1, 2, 3]
                }
            ],
            "mode": "html-template"
        };
    jQuery.ajax({
        beforeSend: function(xhrObj) {
            xhrObj.setRequestHeader("Content-Type", "application/json");
            xhrObj.setRequestHeader("Accept", "application/json");
        },
        type: "POST",
        url: uri,
        data: JSON.stringify(jsonStrJson),
        dataType: "json",
        success: function(json) {
            console.log('Content was loaded with status: ' + json.status);
            jQuery('#aws-target-product-list').html(json.content);
        }
    });
</script>
```