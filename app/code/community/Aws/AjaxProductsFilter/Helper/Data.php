<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CORE_CONFIG_DEFAULT_PREFIX = 'aws_apf/';

    /**
     * @var array
     */
    protected static $_mapOperator = array();
    /**
     * @var array
     */
    protected static $_mapField = array();
    /**
     * @var bool
     */
    protected static $_mapFieldEnabled = false;
    /**
     * @var bool
     */
    protected static $_staticInitFlag = false;
    /** @var Aws_AjaxProductsFilter_Helper_Query */
    protected static $_queryHelper;
    /**
     * @var array
     */
    protected static $_accessibleAttributes = array();
    protected static $_postProcessRules = array();
    protected static $_postProcessedAttributes = array();

    public function __construct()
    {
        static::initStatic();
    }

    public static function getOperatorsMap()
    {
        return [
            'in' => 'in',
            'nin' => 'nin',
            'null' => 'null',
            'notnull' => 'notnull',
            'like' => 'like',
            'nlike' => 'nlike',
            'eq' => 'eq',
            'neq' => 'neq',
            'gt' => 'gt',
            '[gt' => '[gt',
            'gteq' => 'gteq',
            'lt' => 'lt',
            'lt]' => 'lt]',
            'lteq' => 'lteq',
            'between' => 'between',
            'between]' => 'between]',
            '[between' => '[between',
            '[between]' => '[between]',
        ];
    }

    public static function initStatic()
    {
        if (!static::$_staticInitFlag) {
            static::$_queryHelper = Mage::helper('aws_apf/query');
            static::$_mapFieldEnabled = static::getConfigStatic('ajax/map_field_enabled', false);
            static::$_mapOperator = static::getOperatorsMap();
            $rawMapField = static::getConfigStatic('ajax/map_field');
            if (is_string($rawMapField)) {
                static::$_mapField = static::formatSerialised($rawMapField, 'alias', 'attribute', false);
                if (!is_array(static::$_mapField) || !count(static::$_mapField)) {
                    static::$_mapFieldEnabled = false;
                }
            }
            $rawAccessibleAttributes = static::getConfigStatic('general/accessible_attributes');
            if (!is_string($rawAccessibleAttributes)) {
                $rawAccessibleAttributes = array();
            }
            static::$_accessibleAttributes = explode(',', $rawAccessibleAttributes);
            $rawPostprocessRules = static::getConfigStatic('advanced/postprocess_attributes');
            static::$_postProcessRules = static::formatSerialised($rawPostprocessRules, 'attribute');
            static::processPostprocessRules();
        }
    }

    public static function saveToCache($data, $id, $tags = Mage_Cms_Model_Block::CACHE_TAG, $lifeTime = 604800)
    {
        if (is_string($data)) {
            if (!is_array($tags)) {
                $tags = array($tags);
            }
            Mage::app()->saveCache($data, $id, $tags, $lifeTime);
        }
    }

    public static function loadFromCache($id)
    {
        return Mage::app()->loadCache($id);
    }

    public static function removeCache($id)
    {
        Mage::app()->removeCache($id);
    }

    /**
     * @param Varien_Object $config
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    protected function addCategoryFilter($config, $collection)
    {
        $category = Mage::getModel('catalog/category');
        $categories = $config->getCategoryId();
        if (!is_null($categories) && (is_int($categories) || is_numeric($categories))) {
            $categories = array($categories);
        }
        if (is_array($categories) && count($categories)) {
            foreach ($categories as $categoryId) {
                $category->load($categoryId);
                if ($categoryId == $category->getId()) {
                    $collection->addCategoryFilter($category);
                }
            }
        }
    }

    /**
     * @param Varien_Object $config
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    public function addAttributesToSelect($config, $collection)
    {
        $attributes = $config->getData('product_attributes');
        foreach ($attributes as $attribute) {
            $collection->addAttributeToSelect($attribute);
        }
    }

    public function filterProducts($config)
    {
        $collection = $this->initProductCollection();
        $attributes = $config->getAttributes();
        $this->prepareFilter($collection, $attributes);
        $this->addCategoryFilter($config, $collection);
        $this->addAttributesToSelect($config, $collection);
        return $collection;
    }

    public function prepareFilter($collection, array $attributes)
    {
        foreach ($attributes as $attributeFilter) {
            $attributeFilterObject = new Varien_Object();
            $attributeFilterObject->setData($attributeFilter);
            $this->addFilter($collection, $attributeFilterObject);
        }
    }

    public function initProductCollection()
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getResourceCollection();
        return $collection;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param Varien_Object $filter
     * @throws Exception
     */
    protected function addFilter($collection, $filter)
    {
        $operator = $filter->getOperator();
        $value = $filter->getValue();
        $attribute = $filter->getName();

        $operator = $this->mapField($operator, static::$_mapOperator, 'error', '[Operator mapping]');
        if (static::$_mapFieldEnabled) {
            $attribute = $this->mapField($attribute, static::$_mapField, 'error', '[Field mapping]');
        }
        if (!in_array($attribute, static::$_accessibleAttributes)) {
            throw new Exception(
                sprintf('Access to restricted or not existed field "%s".', $attribute)
            );
        }
        $postProcessMap = null;
        $typeOfComparing = 'string';
        if (
            is_array(static::$_postProcessedAttributes) &&
            isset(static::$_postProcessedAttributes[$attribute]) &&
            is_array(static::$_postProcessedAttributes[$attribute])
        ) {
            $postProcessMap = static::$_postProcessedAttributes[$attribute];
            $typeOfComparing = $this->getTypeForFilter(static::$_postProcessRules[$attribute]);
        }

        static::$_queryHelper->buildQueryFragment($collection, $attribute, $value, $operator, $postProcessMap, $typeOfComparing);
    }

    /**
     * @param $attributeRules
     * @return string
     */
    public function getTypeForFilter($attributeRules)
    {
        $type = null;
        if (is_array($attributeRules)) {
            foreach ($attributeRules as $index => $rule) {
                if (is_array($rule)) {
                    if (is_null($type) && isset($rule['type'])) {
                        $type = $rule['type'];
                    } elseif (is_string($type) && isset($rule['type']) && $type !== $rule['type']) {
                        $type = 'string';
                        break;
                    }
                }
            }
        }
        return is_string($type) ? $type : 'string';
    }

    /**
     * @param $field
     * @param $map
     * @param string $notFound
     * @param string $description
     * @return mixed
     * @throws Exception
     */
    public function mapField($field, &$map, $notFound = 'error', $description = '')
    {
        if (!isset($map[$field])) {
            if ('error' === $notFound) {
                throw new Exception(sprintf('Unsupported field found "%s". %s', $field, $description));
            }
        }
        return $map[$field];
    }

    /**
     * @param $raw
     * @param $key
     * @param null $value
     * @param bool $arrayMode
     * @return array
     * @throws Exception
     */
    public static function formatSerialised($raw, $key, $value = null, $arrayMode = true)
    {
        if (is_null($raw)) {
            return array();
        }
        $array = array();
        $result = array();
        $proceedFlag = true;
        try {
            $array = Mage::helper('core/unserializeArray')->unserialize($raw);
        } catch (Exception $exception) {
            $proceedFlag = false;
        }
        if (!$proceedFlag || !is_array($array)) {
            throw new Exception('Enable to unserialize key-pair values.');
        }
        foreach ($array as $item) {
            if (
                is_array($item) && isset($item[$key]) &&
                (is_null($value) || isset($item[$value]))
            ) {
                $keyField = $item[$key];
                if ($arrayMode) {
                    if (!is_array($result[$keyField])) {
                        $result[$keyField] = array();
                    }
                    $result[$keyField][] = is_null($value) ? $item : $item[$value];
                } else {
                    $result[$keyField] = is_null($value) ? $item : $item[$value];
                }
            }
        }
        return $result;
    }

    /**
     * @param $path
     * @param null $default
     * @return mixed|null
     */
    public static function getConfig($path, $default = null)
    {
        return static::getConfigStatic($path, $default);
    }

    /**
     * @param $path
     * @param null $default
     * @return mixed|null
     */
    public static function getConfigStatic($path, $default = null)
    {
        $fullPath = self::CORE_CONFIG_DEFAULT_PREFIX . $path;
        $value = Mage::getStoreConfig($fullPath);
        if (is_null($value)) {
            $value = $default;
        }
        return $value;
    }

    /**
     * @return bool|string
     */
    public static function loadDocFile()
    {
        $buffer = false;
        $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . 'readme.md';
        if (file_exists($path)) {
            try {
                $buffer = file_get_contents($path);
            } catch (Exception $exception) {
                Mage::logException($exception);
            }
        }
        return $buffer;
    }

    /**
     *
     */
    protected static function processPostprocessRules()
    {
        $processedList = array();
        foreach (static::$_postProcessRules as $attributeCode => $postProcessRulesSet) {
            $processedList[$attributeCode] = array();
            if (is_array($postProcessRulesSet)) {
                $buffer = array();
                $bufferType = 'string';
                foreach ($postProcessRulesSet as $postProcessRule) {
                    if ('numeric' === @$postProcessRule['type']) {
                        $bufferType = @$postProcessRule['type'];
                    }
                    $attributeOptions = static::getAttributeOptions($attributeCode);
                    if (is_array($attributeOptions)) {
                        foreach ($attributeOptions as $attributeOption) {
                            if (in_array(@$attributeOption['value'], $processedList[$attributeCode])) {
                                continue;
                            }
                            $originalValue = @$attributeOption['value'];
                            $preged = preg_replace(@$postProcessRule['regexp'], @$postProcessRule['replacement'], @$attributeOption['label']);
                            if (is_string($preged)) {
                                if ('numeric' === @$postProcessRule['type']) {
                                    $preged = /*(double)*/(string)$preged;
                                }
                                $buffer[$preged] = $originalValue;
                                $processedList[$attributeCode][] = $attributeOption['value'];
                            }
                        }
                    }
                }
                if (count($buffer)) {
                    if ('numeric' === $bufferType) {
                        ksort($buffer, SORT_NUMERIC);
                    } else {
                        ksort($buffer, SORT_NATURAL);
                    }
                    static::$_postProcessedAttributes[$attributeCode] = $buffer;
                }
            }
        }
    }

    /**
     * @param $name
     * @return bool
     * @throws Exception
     */
    public static function getAttributeOptions($name)
    {
        $attributeOptions = false;
        try {
            $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)
                ->getFirstItem();
            $attributeId = $attributeInfo->getAttributeId();
            $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
            // TODO Possible add ability to specify store id, currently only Admin store is used
            $attributeOptions = $attribute->getSource()->getAllOptions(false);
        } catch (Exception $exception) {
            throw new Exception(
                sprintf('Can not load attribute "%s", error: %s.', $name, $exception->getMessage())
            );
        }
        return $attributeOptions;
    }

    /**
     * @return array
     */
    public static function getDefaultParameters()
    {
        return array(
            'limit',
            'grid_mode',
            'html_template_toolbar_block_alias',
            'html_template_toolbar_block_name',
            'cache_tag',
            'cache_lifetime',
            'force_load'
        );
    }

    /**
     * @param $config
     * @return string
     */
    public static function generateCacheId($config)
    {
        $appendMinimum = array(
            'attributes',
            'mode',
            'page',
            'limit',
            'grid_mode',
            'html_template_handles',
            'recommended_js'
        );
        $resultStorage = array();
        foreach ($appendMinimum as $item) {
            $resultStorage[] = $config->getData($item);
        }
        $resultString = implode('*', $resultStorage);
        $result = 'AWS-APF-QUERY-' . md5($resultString);
        return $result;
    }
}