<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Helper_Query extends Mage_Core_Helper_Abstract
{
    const COMPARE_MORE_GT = 'gt';
    const COMPARE_MORE_GTEQ = 'gteq';
    const COMPARE_MORE_LT = 'lt';
    const COMPARE_MORE_LTEQ = 'lteq';
    const COMPARE_MORE_EQ = 'eq';

    /**
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param string $field
     * @param mixed $value
     * @param string $operator
     * @param null|array $bufferMap
     * @param string $typeOfComparing
     * @throws Exception
     */
    public function buildQueryFragment(
        $collection,
        $field,
        $value,
        $operator = 'eq',
        $bufferMap = null,
        $typeOfComparing = 'string'
    )
    {
        // if comparable field/attribute is options-based rather then? for example, numeric, replace compare
        // operator with `in`
        $this->postProcessLayer($value, $operator, $bufferMap, $typeOfComparing);
        $modLeft = 0 === strpos($operator,'[') ? '[' : false;
        $modRight = 0 === strpos($operator,']') ? ']' : false;
        $operator = trim($operator, ' []');
        switch ($operator) {
            // TODO Add date filter: from/to/date=true
            case 'between':
                if (!is_array($value) && 1 >= count($value)) {
                    throw new Exception('Values specified for "between" operator in not an array(length = 2');
                }
                $min = (double)array_shift($value);
                $max = (double)array_pop($value);
                $gt = '[' === $modLeft ? 'gteq' : 'gt';
                $lt = '[' === $modRight ? 'lteq' : 'lt';
                $collection->addAttributeToFilter($field, array($gt => $min));
                $collection->addAttributeToFilter($field, array($lt => $max));
                break;
            case 'gteq':
                $modLeft = '[';
            case 'gt':
                $value = (double)$value;
                $gt = '[' === $modLeft ? 'gteq' : 'gt';
                $collection->addAttributeToFilter($field, array($gt => $value));
                break;
            case 'lteq':
                $modRight = ']';
            case 'lt':
                $value = (double)$value;
                $lt = ']' === $modRight ? 'lteq' : 'lt';
                $collection->addAttributeToFilter($field, array($lt => $value));
                break;
            case 'notnull':
                $collection->addAttributeToFilter($field, array('notnull' => true));
                break;
            case 'null':
                $collection->addAttributeToFilter($field, array('null' => true));
                break;
            case 'nlike':
                $collection->addAttributeToFilter($field, array('nlike' => $value));
                break;
            case 'like':
                $collection->addAttributeToFilter($field, array('like' => $value));
                break;
            case 'nin':
                if (!is_array($value)) {
                    $value = array($value);
                }
                $collection->addAttributeToFilter($field, array('nin' => $value));
                break;
            case 'in':
                if (!is_array($value)) {
                    $value = array($value);
                }
                $collection->addAttributeToFilter($field, array('in' => $value));
                break;
            case 'neq':
                $collection->addAttributeToFilter($field, array('neq' => $value));
                break;
            case 'eq':
            default:
                $collection->addAttributeToFilter($field, $value);
                break;
        }
    }

    protected static function sliceArrayByCompare(
        $array,
        $value,
        $compareMode = self::COMPARE_MORE_EQ,
        $typeOfComparing = 'string'
    )
    {
        $index = 0;
        $result = array();
        switch ($typeOfComparing) {
            case 'numeric':
                $value = doubleval($value);
                break;
            case 'string':
                break;
        }
        foreach ($array as $bufferValue => $originalValue)
        {
            switch ($typeOfComparing) {
                case 'numeric':
                    if (is_numeric($bufferValue)) {
                        $bufferValue = doubleval($bufferValue);
                    }
                    break;
                case 'string':
                    break;
            }
            switch ($compareMode) {
                case self::COMPARE_MORE_GTEQ:
                    if ($value <= $bufferValue) {
                        $result = array_slice($array, $index, count($array), true);
                        break;
                    }
                    break;
                case self::COMPARE_MORE_GT:
                    if ($value < $bufferValue) {
                        $result = array_slice($array, $index, count($array), true);
                        break;
                    }
                    break;
                case self::COMPARE_MORE_LTEQ:
                    if ($value <= $bufferValue) {
                        $result = array_slice($array, 0, $index + 1, true);
                        break;
                    }
                    break;
                case self::COMPARE_MORE_LT:
                    if ($value < $bufferValue) {
                        $result = array_slice($array, 0, $index + 1, true);
                        break;
                    }
                    break;
                case self::COMPARE_MORE_EQ:
                default:
                    if ($value == $bufferValue) {
                        $result = array((string)$bufferValue => $originalValue);
                        break;
                    }
                    break;
            }
            $index++;
            if (count($result)) {
                return $result;
            }
        };
        return $result;
    }

    public static function getMinMax($array, &$min, &$max)
    {
        $min = null;
        $max = null;
        if (is_array($array)) {
            $min = min($array);
            $max = max($array);
        }
    }

    public static function getFirstLast($array, &$first, &$last)
    {
        $first = null;
        $last = null;
        if (is_array($array) && count($array)) {
            $first = array_shift($array);
        }
        if (is_array($array) && count($array)) {
            $last = array_pop($array);
        }
    }

    public static function transformCondition($map, &$value, &$operator, $newOperator, $typeOfComparing = 'string')
    {
        $bufferMap = static::sliceArrayByCompare(
            $map,
            $value,
            $newOperator,
            $typeOfComparing
        );
        $operator = 'in';
        $value = array_values($bufferMap);
    }

    public function postProcessLayer(&$value, &$operator, $map, $typeOfComparing = 'string')
    {
        if (!is_array($map) || !count($map)) {
            return ;
        }
        $modLeft = 0 === strpos($operator,'[') ? '[' : false;
        $modRight = strlen($operator) - 1 === strpos($operator,']') ? ']' : false;
        $operator = trim($operator, ' []');
        $newOperator = null;
        switch ($operator) {
            case 'between':
                $first = null;
                $last = null;
                static::getFirstLast($value, $first, $last);
                if (!is_null($first) && !is_null($last)) {
                    $bufferMap = static::sliceArrayByCompare(
                        $map,
                        $first,
                        $modLeft ? self::COMPARE_MORE_GTEQ : self::COMPARE_MORE_GT
                    );
                    $bufferMap = static::sliceArrayByCompare(
                        $bufferMap,
                        $last,
                        $modRight ? self::COMPARE_MORE_LTEQ : self::COMPARE_MORE_LT
                    );
                    $operator = 'in';
                    $value = array_values($bufferMap);
                }
                // return here
                return;
            case 'gt':
                $newOperator = $modRight ? self::COMPARE_MORE_LTEQ : self::COMPARE_MORE_LT;
                break;
            case 'gteq':
                $newOperator = self::COMPARE_MORE_GTEQ;
                break;
            case 'lt':
                $newOperator = $modRight ? self::COMPARE_MORE_LTEQ : self::COMPARE_MORE_LT;
                break;
            case 'lteq':
                $newOperator = self::COMPARE_MORE_LTEQ;
                break;
            case 'eq':
            default:
                $newOperator = self::COMPARE_MORE_EQ;
        }
        if (!is_null($newOperator)) {
            static::transformCondition($map, $value, $operator, $newOperator, $typeOfComparing);
        }
    }
}