<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_Helper_Mode extends Mage_Core_Helper_Abstract
{
    const JS_MASK = '<script type="text/javascript">{{content}}</script>';

    public static function formatMethodName($alias)
    {
        if (!is_string($alias) || !strlen($alias)) {
            return false;
        }
        $parts = explode('-', $alias);
        $parts = array_map('ucfirst', $parts);
        array_unshift($parts, 'get');
        $methodName = implode('', $parts);
        if (!method_exists('Aws_AjaxProductsFilter_Helper_Mode', $methodName)) {
            return false;
        }
        return $methodName;
    }

    /**
     * @param Varien_Object $config
     * @return string
     */
    public function getHtmlTemplate($config)
    {
        $result = '';
        /** @var null|Mage_Core_Block_Template $targetBlock */
        $targetBlock = null;
        $template = $config->getTemplate();
        switch ($template) {
            case 'products-list':
            default:
                // seems that there is no need in emulation now
                $handles = $config->getData('html_template_handles');
                $this->prepareLayout($handles);
                $pseudoLayer = Mage::getModel('aws_apf/filterLayer');
                $pseudoLayer->setCollection($config->getProductCollection());
                Mage::register('current_layer', $pseudoLayer);
                // for safety
                Mage::register('current_category', Mage::getModel('catalog/category'));
                $layout = Mage::app()->getLayout();
                $layout->generateXml();
                $layout->generateBlocks();
                $targetBlock = $layout->getBlock('product_list');
                if ($targetBlock) {
                    Aws_AjaxProductsFilter_Model_Observer::skipBlock('toolbar', 1);
                    Aws_AjaxProductsFilter_Model_Observer::toggleMode(true);
                    $toolbarBlock = $targetBlock->getChild('product_list_toolbar');
                    if ($toolbarBlock) {
                        $toolbarBlock->setData('_current_limit', $config->getLimit());
                        $toolbarBlock->setData('_current_grid_mode', $config->getGridMode());
                    }
                    $result = $targetBlock->toHtml();
                    Aws_AjaxProductsFilter_Model_Observer::toggleMode(false);
                }
                break;
        }
        $result = $this->appendRecommendedJs($result, $config);
        return $result;
    }

    protected function appendRecommendedJs($result, $config)
    {
        if (
            $config->getData('recommended_js_attach') &&
            is_string($config->getData('recommended_js'))
        ) {
            $buffer = Aws_MetaBase_Helper_Data::replaceStringByMask(
                self::JS_MASK,
                array('content' => $config->getData('recommended_js'))
            );
            $result .= $buffer;
        }
        return $result;
    }

    protected function createBlock($blockName, $template)
    {
        $layout = $this->getLayout();
        $block = $layout->createBlock($blockName, $template);
        return $block;
    }

    protected function prepareLayout($handles)
    {
        $layout = Mage::app()->getLayout();
        $update = $layout->getUpdate();
        $layout->getUpdate()->resetHandles();
        $update->setCacheId(uniqid('new-cache-' . (string)time()));
        $update->load($handles);
    }

    protected function emulateAppLayout($storeId, $handles)
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
        $this->prepareLayout($handles);
        return $initialEnvironmentInfo;
    }

    protected function stopEmulateAppLayout($initialEnvironmentInfo)
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    }
}