<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class Aws_AjaxProductsFilter_SearchController extends Mage_Core_Controller_Front_Action
{
    const MESSAGE_ACCESS_DENIED = 'Access denied';
    public $config;
    /** @var array */
    protected $_allowedModes = [];
    /** @var Aws_AjaxProductsFilter_Helper_Mode */
    protected $_modeHelper;
    /** @var Aws_AjaxProductsFilter_Helper_Data */
    protected $_dataHelper;

    public function _construct()
    {
        parent::_construct();
        $this->_modeHelper = Mage::helper('aws_apf/mode');
        $this->_dataHelper= Mage::helper('aws_apf');
        $this->initConfiguration();
    }

    public function executeAction()
    {
        $resultBody = '';
        $info = array();
        if ($this->initController()) {
            $resultBody = $this->processRequest($info);
        }
        if (!$this->config->getProceedFlag() || !is_string($resultBody)) {
            $resultBody = $this->processError();
        }
        $responseBody = $this->packResponse($resultBody, $info);
        $this->_response->setBody($responseBody);
    }

    protected function processError()
    {
        $messages = $this->config->getMessages();
        if (!is_array($messages)) {
            $messages = array(
                'Unknown error occurred.'
            );
        }
        $messageConcat = implode(PHP_EOL, $messages);
        return $messageConcat;
    }

    protected function processRequest(&$info = array())
    {
        $cacheId = $this->config->getCacheId();
        $cache = Aws_AjaxProductsFilter_Helper_Data::loadFromCache($cacheId);
        if ($cache) {
            if (!$this->config->getForceLoad()) {
                $info['cache'] = 'Loaded from cache.';
                return $cache;
            } else {
                $info['cache'] = 'Cache exists, but force load parameter is specified.';
            }
        }
        $mode = $this->config->getMode();
        $methodName = Aws_AjaxProductsFilter_Helper_Mode::formatMethodName($mode);
        if (!$methodName) {
            $errorMessage = sprintf('Specified mode "%s" doesn\'t exists.', $mode);
            $this->appendErrorMessage($errorMessage);
            return false;
        }
        try {
            $collection = $this->_dataHelper->filterProducts($this->config);
            $this->config->setProductCollection($collection);
            $result = call_user_func(array($this->_modeHelper, $methodName), $this->config);
            $info['products_count_loaded'] = $collection->count();
            $info['products_count_total'] = $collection->getSize();
            $info['recommended_js'] = $this->config->getData('recommended_js');
        } catch (Exception $exception) {
            $errorMessage = sprintf('Error occurred while executing "%s" generation.', $mode);
            $this->appendErrorMessage($errorMessage);
            $this->appendErrorMessage($exception->getMessage());
            return false;
        }
        Aws_AjaxProductsFilter_Helper_Data::saveToCache(
            $result,
            $cacheId,
            $this->config->getCacheTag(),
            $this->config->getCacheLifetime()
        );
        $info['cache'] = 'Results were saved to cache.';
        return $result;
    }

    public function packResponse($resultBody, $info = array())
    {
        $resultRaw = array(
            'status' => $this->config->getProceedFlag(),
            'info' => $info,
            'content' => $resultBody
        );
        return Mage::helper('core')->jsonEncode($resultRaw);
    }

    protected function initModes()
    {
        $this->_allowedModes = array(
            'html-template'
        );
    }

    public function formkeyAction()
    {
        $result = self::MESSAGE_ACCESS_DENIED;
        $this->config->setProceedFlag(false);
        if ($this->config->getDevMode()) {
            $result = Mage::getSingleton('core/session')->getFormKey();
            $this->config->setProceedFlag(true);
        }
        $responseBody = $this->packResponse($result);
        $this->_response->setBody($responseBody);
    }

    protected function initConfiguration()
    {
        $rawConfig = array(
            'form_key' => '',
            'attributes' => array(),
            'mode' => 'html-template',
            'page' => 1,
            'limit' => 'all',
            'grid_mode' => 'grid',
            'html_template_toolbar_block_alias' => 'toolbar',
            'html_template_toolbar_block_name' => 'product_list_toolbar',
            'cache_tag' => 'cms_block',
            'cache_lifetime' => 604800,
            'force_load' => false,
            'category_id' => []
        );
        $devMode = (int)Aws_AjaxProductsFilter_Helper_Data::getConfig('advanced/dev_mode', 0);
        $recommendedJs = Aws_AjaxProductsFilter_Helper_Data::getConfig('advanced/recommended_js_html', '');
        $recommendedJsAttach = Aws_AjaxProductsFilter_Helper_Data::getConfig(
            'advanced/recommended_js_html_enabled',
            false
        );
        $htmlTemplateHandlesRaw = Aws_AjaxProductsFilter_Helper_Data::getConfig(
            'advanced/html_template_handles',
            'default,catalog_category_default,catalog_category_view'
        );
        $htmlTemplateHandles = preg_split('/[\r\n\,\|]+/', $htmlTemplateHandlesRaw);
        $productAttributesToSelectRaw = Aws_AjaxProductsFilter_Helper_Data::getConfig(
            'advanced/product_attributes',
            'name,price,image,image_label,description,short_description,small_image'
        );
        $productAttributesToSelect = explode(',', $productAttributesToSelectRaw);
        $productAttributesToSelect = array_map('trim', $productAttributesToSelect);
        $defaultSettingsRaw = Aws_AjaxProductsFilter_Helper_Data::getConfig('general/default_settings', '');
        $defaultSettings = Aws_AjaxProductsFilter_Helper_Data::formatSerialised(
            $defaultSettingsRaw,
            'parameter',
            'value',
            false
        );
        $rawFinalConfig = array(
            'messages' => array(),
            'proceed_flag' => false,
            'template' => 'products-list',
            'product_collection' => new Varien_Object(),
            'store_id' => Mage::app()->getStore()->getId(),
            'recommended_js' => $recommendedJs,
            'recommended_js_attach' => $recommendedJsAttach,
            'html_template_handles' => $htmlTemplateHandles,
            'dev_mode' => $devMode,
            'product_attributes' => $productAttributesToSelect
        );
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $this->getRequest();
        $rawParams = $this->detectRequest($request);
        $rawConfig = array_merge($rawConfig, $defaultSettings, $rawParams, $rawFinalConfig);
        $this->config = new Varien_Object();
        $this->config->setData($rawConfig);
        $this->config->setCacheId(Aws_AjaxProductsFilter_Helper_Data::generateCacheId($this->config));
        // TODO Get available modes from helper Mode class
        $this->_allowedModes = array('html-template');
    }

    public function detectRequest($request)
    {
        $contentType = $request->getHeader('Content-Type');
        $contentType = strtolower($contentType);
        $content = array();
        switch ($contentType) {
            case 'application/json':
            default:
                $contentRaw = $request->getRawBody();
                try {
                    $content = Mage::helper('core')->jsonDecode($contentRaw, Zend_Json::TYPE_ARRAY);
                } catch (Exception $exception) {
                    return array();
                }
        }
        return is_array($content) ? $content : array();
    }

    private function initController()
    {
        $messages = array();
        $proceedFlag = true;
        // 1. check form key
        if (!$this->checkFormKey($this->config->getFormKey())) {
            $messages[] = 'Invalid form key supplied to request.';
            $proceedFlag = false;
        }
        // 2. check minimal request data
        if (!$this->checkRequest()) {
            $messages[] = 'Bad request: some fields are missing or invalid ajax request performed.';
            $proceedFlag = false;
        }
        // 3. if request is ok - proceed to generating response
        // 3.1 load layout
        // TODO Remove this as no need
        // $this->loadLayout();
        // 3.2 prepare and return configuration object
        $this->prepareConfiguration($proceedFlag);
        foreach ($messages as $message) {
            $this->appendErrorMessage($message);
        }
        return $proceedFlag;
    }

    protected function checkFormKey($key)
    {
        $actualFormKey = Mage::getSingleton('core/session')->getFormKey();
        if ($actualFormKey !== $key) {
            return false;
        }
        return true;
    }

    protected function checkRequest()
    {
        $request = $this->getRequest();
        $isAjax = $request->isAjax() || $request->isPost();
        $attributes = $this->config->getAttributes();
        $mode = $this->config->getMode();
        if (
            $isAjax &&
            is_array($attributes) && count($attributes) &&
            is_string($mode) && in_array($mode, $this->_allowedModes)
        ) {
            return true;
        }
        return false;
    }

    protected function prepareConfiguration($proceedFlag = false, $messages= array())
    {
        $this->config->setProceedFlag($proceedFlag);
        $this->config->setMessages($messages);
    }

    public function appendErrorMessage($message)
    {
        $messages = $this->config->getMessages();
        $messages[] = $message;
        $this->config->setMessages($messages);
        $this->config->setProceedFlag(false);
    }
}